const BASE_URL = "https://capstone-2-atienza.onrender.com/";
//const BASE_URL = "http://localhost:4000/";

export function login(email, password) {
  return fetch(`${BASE_URL}users/login`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      email: email,
      password: password,
    }),
  });
}

export function googleLogin(jsonData) {
  return fetch(`${BASE_URL}google`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      data: jsonData,
    }),
  });
}

export function getAllUsers() {
  return fetch(`${BASE_URL}users/`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
}

export function makeAdmin(userId) {
  return fetch(`${BASE_URL}users/updateAdmin/${userId}`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
}

export function removeAdmin(userId) {
  return fetch(`${BASE_URL}users/updateAdmin/${userId}`, {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
}

export function getUserDetails(token) {
  return fetch(`${BASE_URL}users/getUserDetails`, {
    headers: {
      Authorization: `Bearer ${token ? token : localStorage.getItem("token")}`,
    },
  });
}

export function register(firstName, lastName, email, password, mobileNo) {
  return fetch(`${BASE_URL}users/register`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      firstName: firstName,
      lastName: lastName,
      email: email,
      password: password,
      mobileNo: mobileNo,
    }),
  });
}

export function getAllProducts() {
  return fetch(`${BASE_URL}products/all`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
}

export function getAllActiveProducts() {
  return fetch(`${BASE_URL}products/`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  });
}

export function createProduct(name, description, price) {
  return fetch(`${BASE_URL}products`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
    body: JSON.stringify({
      name: name,
      description: description,
      price: price,
    }),
  });
}

export function getSpecificProduct(id) {
  return fetch(`${BASE_URL}products/${id}`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  });
}

export function archiveProduct(id) {
  return fetch(`${BASE_URL}products/${id}/archive`, {
    method: "PATCH",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
}

export function activateProduct(id) {
  return fetch(`${BASE_URL}products/${id}/activate`, {
    method: "PATCH",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
}

export function updateProduct(id, photo, name, description, price) {
  return fetch(`${BASE_URL}products/${id}`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
    body: JSON.stringify({
      photo: photo,
      name: name,
      description: description,
      price: price,
    }),
  });
}

export function addToCart(id, quantity) {
  return fetch(`${BASE_URL}products/${id}/cart`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
    body: JSON.stringify({
      quantity: quantity,
    }),
  });
}

export function editCart(id, quantity) {
  return fetch(`${BASE_URL}products/${id}/cart`, {
    method: "PATCH",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
    body: JSON.stringify({
      quantity: quantity,
    }),
  });
}

export function deleteFromCart(id) {
  return fetch(`${BASE_URL}products/${id}/cart`, {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
}

export function checkoutCart(
  productIds,
  totalAmount,
  shippingFee,
  deliveryAddress
) {
  return fetch(`${BASE_URL}users/checkout`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
    body: JSON.stringify({
      productIds: productIds,
      totalAmount: totalAmount,
      shippingFee: shippingFee,
      deliveryAddress: deliveryAddress,
    }),
  });
}

export function getUserOrders() {
  return fetch(`${BASE_URL}orders/getUserOrders`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
}

export function getAllOrders() {
  return fetch(`${BASE_URL}orders`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
}

export function getRegions() {
  return fetch(`https://ph-locations-api.buonzz.com/v1/regions`, {});
}

export function getProvinces() {
  return fetch(`https://ph-locations-api.buonzz.com/v1/provinces`, {});
}

export function getCities() {
  return fetch(`https://ph-locations-api.buonzz.com/v1/cities`, {});
}
