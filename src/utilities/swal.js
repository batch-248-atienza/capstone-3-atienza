import Swal from "sweetalert2";

export const customAlert = Swal.mixin({
  toast: true,
  position: "top-end",
  showConfirmButton: false,
  timer: 3000,
});

export const customConfirmAlert = Swal.mixin({
  toast: true,
  position: "center",
  showConfirmButton: true,
  showDenyButton: true,
});

export const loadingAlert = Swal.mixin({
  allowEscapeKey: false,
  allowOutsideClick: false,
  showConfirmButton: false,
  didOpen: () => {
    Swal.showLoading();
  },
});
