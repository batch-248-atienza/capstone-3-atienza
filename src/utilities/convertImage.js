export async function convertToBase64(file) {
  const fileReader = new FileReader();
  fileReader.readAsDataURL(file);

  let isFinished = false;
  let base64Data = "";

  fileReader.onload = () => {
    base64Data = fileReader.result;
    isFinished = true;
  };

  while (!isFinished) {
    await new Promise((resolve) => setTimeout(resolve, 100));
  }

  return base64Data;
}
