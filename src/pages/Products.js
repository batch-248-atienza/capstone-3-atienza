import { useState, useEffect } from "react";
import { Container, Card, Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";

import { getAllActiveProducts } from "../api";
import { customAlert, loadingAlert } from "../utilities/swal";

export default function Products() {
  const [products, setProducts] = useState([]);

  async function getData() {
    loadingAlert.fire({ title: "Retrieving data" });
    try {
      const response = await getAllActiveProducts();
      const data = await response.json();
      loadingAlert.close();

      if (data.status === true) {
        setProducts(data.message);
      } else {
        setProducts(null);
      }
    } catch (e) {
      loadingAlert.close();
      customAlert.fire({
        title: "An Error occured!",
        icon: "error",
      });
    }
  }

  useEffect(() => {
    getData();
  }, []);

  return (
    <Container fluid>
      <Row>
        <h6>Products</h6>
        {products !== undefined && products !== null
          ? products.map((product, index) => {
              return (
                <Col xs={6} sm={6} md={3} lg={2}>
                  <Card
                    id="productCard"
                    as={Link}
                    to={`/products/${product._id}`}
                  >
                    <Card.Img src={product.photo} />
                    <Card.Body>
                      <Card.Title>{product.name}</Card.Title>
                      <Card.Subtitle>
                        ₱{product.price}
                        <span>
                          {product.orders.length > 0
                            ? product.orders.reduce(
                                (total, order) => total + order.quantity,
                                0
                              ) + " sold"
                            : 0 + " sold"}
                        </span>
                      </Card.Subtitle>
                      <Card.Text className="text-truncate">
                        {product.description}
                      </Card.Text>
                    </Card.Body>
                  </Card>
                </Col>
              );
            })
          : null}
      </Row>
    </Container>
  );
}
