import { useEffect, useContext } from "react";
import { Navigate } from "react-router-dom";
import UserContext from "../UserContext";

import { customAlert } from "../utilities/swal";

export default function Logout() {
  const { setUser, unsetUser, setCart } = useContext(UserContext);

  unsetUser();

  useEffect(() => {
    setUser({ id: null });
    setCart(null);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [setUser]);

  customAlert.fire({
    title: "Logout Successful!",
    icon: "success",
  });

  return <Navigate to="/" />;
}
