import { useState, useEffect, useContext } from "react";
import UserContext from "../UserContext";
import DashboardNavBar from "../components/DashboardNavBar";

import { Container, Row, Card } from "react-bootstrap";
import { Navigate } from "react-router-dom";

import { getAllUsers, makeAdmin, removeAdmin } from "../api";
import {
  customAlert,
  loadingAlert,
  customConfirmAlert,
} from "../utilities/swal";

import { AiOutlineCheckSquare, AiOutlineBorder } from "react-icons/ai";

export default function Users() {
  const [users, setUsers] = useState({});

  async function handleMakeAdmin(user, index) {
    const customConfirmAlertSelection = await customConfirmAlert.fire({
      title: "Are you sure you make user an admin?",
      icon: "question",
      confirmButtonText: "Yes",
      denyButtonText: "No",
    });

    if (customConfirmAlertSelection.isConfirmed) {
      loadingAlert.fire({ title: "Making admin" });

      try {
        const response = await makeAdmin(user._id);
        const data = await response.json();
        loadingAlert.close();
        console.log(data);
        if (data.status === true) {
          customAlert.fire({
            title: "Status Updated",
            icon: "success",
          });

          refreshData();
        } else {
          customAlert.fire({
            title: "An Error occured!",
            icon: "error",
          });
        }
      } catch (e) {
        loadingAlert.close();
        customAlert.fire({
          title: "An Error occured!",
          icon: "error",
        });
      }
    }
  }

  async function handleRemoveAdmin(user, index) {
    const customConfirmAlertSelection = await customConfirmAlert.fire({
      title: "Are you sure you remove user as an admin?",
      icon: "question",
      confirmButtonText: "Yes",
      denyButtonText: "No",
    });

    if (customConfirmAlertSelection.isConfirmed) {
      loadingAlert.fire({ title: "Removing admin" });

      try {
        const response = await removeAdmin(user._id);
        const data = await response.json();
        loadingAlert.close();
        console.log(data);
        if (data.status === true) {
          customAlert.fire({
            title: "Status Updated",
            icon: "success",
          });

          refreshData();
        } else {
          customAlert.fire({
            title: "An Error occured!",
            icon: "error",
          });
        }
      } catch (e) {
        loadingAlert.close();
        customAlert.fire({
          title: "An Error occured!",
          icon: "error",
        });
      }
    }
  }

  async function getData() {
    loadingAlert.fire({ title: "Retrieving data" });
    try {
      const response = await getAllUsers();
      const data = await response.json();
      loadingAlert.close();

      if (data.status === true) {
        setUsers(data.message);
      } else {
        setUsers(null);
      }
    } catch (e) {
      loadingAlert.close();
      customAlert.fire({
        title: "An Error occured!",
        icon: "error",
      });
    }
  }

  async function refreshData() {
    try {
      const response = await getAllUsers();
      const data = await response.json();

      if (data.status === true) {
        setUsers(data.message);
      } else {
        setUsers(null);
      }
    } catch (e) {
      customAlert.fire({
        title: "An Error occured!",
        icon: "error",
      });
    }
  }

  useEffect(() => {
    getData();
  }, []);

  const { user } = useContext(UserContext);

  return user.id === null ? (
    <Navigate to="/" />
  ) : user.isAdmin !== true ? (
    <Navigate to="/dashboard" />
  ) : (
    <Container>
      <Row>
        <DashboardNavBar />
      </Row>
      <Row>
        {users.length > 0
          ? users.map((user, index) => {
              return (
                <Card className="mb-3 userCard">
                  <Card.Body>
                    <Card.Title>
                      {user.firstName}&nbsp;{user.lastName}
                      <span>{user.email}</span>
                    </Card.Title>
                    <Card.Subtitle>{user.mobileNo}</Card.Subtitle>
                    <Card.Text>
                      Admin:{" "}
                      {user.isAdmin ? (
                        <AiOutlineCheckSquare
                          className="svgPointer"
                          onClick={() => handleRemoveAdmin(user, index)}
                        />
                      ) : (
                        <AiOutlineBorder
                          className="svgPointer"
                          onClick={() => handleMakeAdmin(user, index)}
                        />
                      )}
                    </Card.Text>
                  </Card.Body>
                </Card>
              );
            })
          : null}
      </Row>
    </Container>
  );
}
