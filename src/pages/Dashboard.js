import { useState, useContext, useEffect } from "react";
import { Navigate, Link } from "react-router-dom";
import { Container, Row, Table, Alert, Button } from "react-bootstrap";
import {
  AiFillPlusCircle,
  AiFillCloseCircle,
  AiFillCheckCircle,
  AiOutlineCheckSquare,
  AiOutlineBorder,
} from "react-icons/ai";
import { motion, AnimatePresence } from "framer-motion";

import UserContext from "../UserContext";

import {
  getAllProducts,
  createProduct,
  archiveProduct,
  activateProduct,
  updateProduct,
} from "../api";
import {
  customAlert,
  customConfirmAlert,
  loadingAlert,
} from "../utilities/swal";
import { convertToBase64 } from "../utilities/convertImage";
import DashboardNavBar from "../components/DashboardNavBar";

export default function Login() {
  const { user } = useContext(UserContext);

  const [products, setProducts] = useState([]);
  const [tempProducts, setTempProducts] = useState([]);
  const [isEditing, setIsEditing] = useState(false);
  const [editingIndex, setEditingIndex] = useState("");

  async function getData() {
    loadingAlert.fire({ title: "Retrieving data" });
    try {
      const response = await getAllProducts();
      const data = await response.json();
      loadingAlert.close();

      if (data.status === true) {
        setProducts(data.message);
        setTempProducts(data.message);
      } else {
        setProducts(null);
        setTempProducts(null);
      }
    } catch (e) {
      loadingAlert.close();
      customAlert.fire({
        title: "An Error occured!",
        icon: "error",
      });
    }
  }

  async function refreshData() {
    try {
      const response = await getAllProducts();
      const data = await response.json();

      if (data.status === true) {
        setProducts(data.message);
        setTempProducts(data.message);
      } else {
        setProducts(null);
        setTempProducts(null);
      }
    } catch (e) {
      customAlert.fire({
        title: "An Error occured!",
        icon: "error",
      });
    }
  }

  function addRow(index) {
    setIsEditing(true);
    setEditingIndex(index);
    const newProduct = {
      name: "",
      description: "",
      isActive: true,
      price: 0,
    };
    setTempProducts((prevState) => [...prevState, newProduct]);
  }

  function handleChange(e, index, key) {
    setTempProducts((prevProducts) => {
      const products = [...prevProducts];
      products[index] = {
        ...products[index],
        [key]: e.target.value,
      };
      return products;
    });
  }

  async function handleNewPhoto(e, index) {
    const file = e.target.files[0];
    const base64 = await convertToBase64(file);

    await setTempProducts((prevProducts) => {
      const products = [...prevProducts];

      products[index].photo = base64;
      return products;
    });
  }

  async function saveChanges(index) {
    try {
      const product = tempProducts[index];
      let type;
      let response;
      tempProducts.length === products.length
        ? (type = "edit")
        : (type = "create");

      if (
        product.name === "" ||
        product.description === "" ||
        product.price === 0
      ) {
        customAlert.fire({
          title: "Cannot have blank fields!",
          icon: "error",
        });
      } else {
        loadingAlert.fire({ title: "Saving" });

        type === "edit"
          ? (response = await updateProduct(
              product._id,
              product.photo,
              product.name,
              product.description,
              product.price
            ))
          : (response = await createProduct(
              product.name,
              product.description,
              product.price
            ));
        const data = await response.json();
        loadingAlert.close();

        console.log(data);

        if (data.status === true) {
          refreshData();
          setIsEditing(false);

          type === "edit"
            ? customAlert.fire({
                title: "Product Updated!",
                icon: "success",
              })
            : customAlert.fire({
                title: "New Product Created!",
                icon: "success",
              });
        } else {
          setTempProducts(products);
          setIsEditing(false);
          customAlert.fire({
            title: "An Error occured!",
            icon: "error",
          });
        }
      }
    } catch (e) {
      await setTempProducts(products);
      setIsEditing(false);
      customAlert.fire({
        title: "An Error occured!",
        icon: "error",
      });
    }
  }

  function cancelChanges() {
    setTempProducts(products);
    setIsEditing(false);
    setEditingIndex("");
  }

  async function handleArchive(product, index) {
    const customConfirmAlertSelection = await customConfirmAlert.fire({
      title: "Are you sure you want to archive?",
      icon: "question",
      confirmButtonText: "Yes",
      denyButtonText: "No",
    });

    if (customConfirmAlertSelection.isConfirmed) {
      loadingAlert.fire({ title: "Archiving" });

      try {
        const response = await archiveProduct(product._id);
        const data = await response.json();
        loadingAlert.close();
        console.log(data);
        if (data.status === true) {
          customAlert.fire({
            title: "Product Archived",
            icon: "success",
          });

          refreshData();
        } else {
          customAlert.fire({
            title: "An Error occured!",
            icon: "error",
          });
        }
      } catch (e) {
        loadingAlert.close();
        customAlert.fire({
          title: "An Error occured!",
          icon: "error",
        });
      }
    }
  }

  async function handleActivate(product, index) {
    const customConfirmAlertSelection = await customConfirmAlert.fire({
      title: "Are you sure you want to activate?",
      icon: "question",
      confirmButtonText: "Yes",
      denyButtonText: "No",
    });

    if (customConfirmAlertSelection.isConfirmed) {
      loadingAlert.fire({ title: "Activating" });

      try {
        const response = await activateProduct(product._id);
        const data = await response.json();
        loadingAlert.close();

        if (data.status === true) {
          customAlert.fire({
            title: "Product Activated",
            icon: "success",
          });

          refreshData();
        } else {
          customAlert.fire({
            title: "An Error occured!",
            icon: "error",
          });
        }
      } catch (e) {
        loadingAlert.close();
        customAlert.fire({
          title: "An Error occured!",
          icon: "error",
        });
      }
    }
  }

  function handleEdit(product, index) {
    setEditingIndex(index);
    setIsEditing(true);
  }

  useEffect(() => {
    getData();
  }, []);

  return user.id === null ? (
    <Navigate to="/" />
  ) : user.isAdmin !== true ? (
    <Navigate to="/dashboard" />
  ) : (
    <Container>
      <Row>
        <DashboardNavBar />
      </Row>
      <Row>
        {tempProducts !== undefined && tempProducts !== null ? null : (
          <Alert key={"danger"} variant={"danger"}>
            Failed to retrieve products
          </Alert>
        )}
        <AnimatePresence>
          {isEditing ? (
            <motion.div
              transition={{ duration: 0.2 }}
              initial={{ opacity: 0, scale: 0 }}
              animate={{ x: 0, y: 0, opacity: 1, scale: 1 }}
            >
              <Alert key={"warning"} variant={"warning"}>
                <Alert.Heading>You are currently editing</Alert.Heading>
                <p>Changes will not take effect until you save.</p>
              </Alert>
            </motion.div>
          ) : null}
        </AnimatePresence>
        <Table striped size="sm" id="productsTable">
          <thead>
            <tr>
              <th>#</th>
              <th>Image</th>
              <th>Product Name</th>
              <th>Product Description</th>
              <th>Status</th>
              <th>Price</th>
              <th>View</th>
              <th>Edit</th>
            </tr>
          </thead>
          <tbody>
            {tempProducts !== undefined && tempProducts !== null
              ? tempProducts.map((product, index) => {
                  return (
                    <tr key={index}>
                      <td>{index + 1}</td>
                      {index >= products.length ||
                      (isEditing && editingIndex === index) ? (
                        <td className="imageUploadProducts">
                          <label for="image-upload">
                            <img
                              src={product.photo}
                              alt="product"
                              className="productPhoto"
                            />
                          </label>
                          <input
                            onChange={(e) => handleNewPhoto(e, index)}
                            id="image-upload"
                            type="file"
                          />
                        </td>
                      ) : (
                        <td>
                          <img
                            src={product.photo}
                            alt="product"
                            className="productPhoto"
                          />
                        </td>
                      )}

                      {index >= products.length ||
                      (isEditing && editingIndex === index) ? (
                        <td>
                          <input
                            className="productEdit"
                            value={product.name}
                            onChange={(e) => handleChange(e, index, "name")}
                          />
                        </td>
                      ) : (
                        <td>{product.name}</td>
                      )}
                      {index >= products.length ||
                      (isEditing && editingIndex === index) ? (
                        <td>
                          <textarea
                            className="productEdit"
                            value={product.description}
                            onChange={(e) =>
                              handleChange(e, index, "description")
                            }
                          />
                        </td>
                      ) : (
                        <td>{product.description}</td>
                      )}
                      <td>
                        {product.isActive ? (
                          <AiOutlineCheckSquare
                            className="svgPointer"
                            onClick={() => handleArchive(product, index)}
                          />
                        ) : (
                          <AiOutlineBorder
                            className="svgPointer"
                            onClick={() => handleActivate(product, index)}
                          />
                        )}
                      </td>
                      {index >= products.length ||
                      (isEditing && editingIndex === index) ? (
                        <td>
                          <input
                            className="productEdit"
                            value={product.price}
                            onChange={(e) => handleChange(e, index, "price")}
                            type="number"
                          />
                        </td>
                      ) : (
                        <td>₱{product.price}</td>
                      )}
                      <td>
                        <Button
                          variant={
                            isEditing && editingIndex === index
                              ? "secondary"
                              : "success"
                          }
                          className="dashboardButton"
                          as={Link}
                          to={`/products/${product._id}`}
                        >
                          View
                        </Button>
                      </td>
                      <td id="editButtonTable">
                        <Button
                          variant={
                            isEditing && editingIndex === index
                              ? "secondary"
                              : "info"
                          }
                          className="dashboardButton"
                          onClick={() => handleEdit(product, index)}
                        >
                          Edit
                        </Button>
                        {isEditing && editingIndex === index ? (
                          <>
                            <AiFillCheckCircle
                              size={20}
                              id="saveChanges"
                              onClick={() => saveChanges(index)}
                            />
                            <AiFillCloseCircle
                              size={20}
                              id="cancelChanges"
                              onClick={cancelChanges}
                            />
                          </>
                        ) : null}
                        {!isEditing && index === products.length - 1 ? (
                          <AiFillPlusCircle
                            size={20}
                            id="addRow"
                            onClick={() => addRow(tempProducts.length)}
                          />
                        ) : null}
                      </td>
                    </tr>
                  );
                })
              : null}
          </tbody>
        </Table>
      </Row>
    </Container>
  );
}
