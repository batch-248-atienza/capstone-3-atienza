import { useState, useEffect, useContext } from "react";
import UserContext from "../UserContext";
import { Link, Navigate } from "react-router-dom";

import {
  getAllActiveProducts,
  editCart,
  deleteFromCart,
  getUserDetails,
} from "../api";
import { customAlert, loadingAlert } from "../utilities/swal";

import {
  AiOutlineCheckSquare,
  AiOutlineBorder,
  AiOutlineDelete,
} from "react-icons/ai";
import { ImSpoonKnife } from "react-icons/im";

import { Container, Table, Button } from "react-bootstrap";

export default function Cart() {
  const { user, cart, setCart, forCheckout, setForCheckout } =
    useContext(UserContext);
  const [products, setProducts] = useState([]);

  async function getData() {
    loadingAlert.fire({ title: "Retrieving bowl" });
    try {
      const response = await getAllActiveProducts();
      const data = await response.json();
      loadingAlert.close();

      if (data.status === true) {
        setProducts(data.message);
      } else {
        setProducts(null);
      }
    } catch (e) {
      loadingAlert.close();
      customAlert.fire({
        title: "An Error occured!",
        icon: "error",
      });
    }
  }

  async function subtract(product) {
    try {
      if (product.quantity !== 1) {
        await editCart(product.productId, product.quantity - 1);
        const response = await getUserDetails();
        const data = await response.json();
        setCart(data.message.cart);
      }
    } catch (e) {
      customAlert.fire({
        title: "An Error occured!",
        icon: "error",
      });
    }
  }

  async function add(product) {
    try {
      await editCart(product.productId, product.quantity + 1);
      const response = await getUserDetails();
      const data = await response.json();
      setCart(data.message.cart);
    } catch (e) {
      customAlert.fire({
        title: "An Error occured!",
        icon: "error",
      });
    }
  }

  function addToCheckout(index) {
    setForCheckout((prevArray) => [...prevArray, cart[index].productId]);
  }

  function removeToCheckout(index) {
    setForCheckout(
      forCheckout.filter((item) => item !== cart[index].productId)
    );
  }

  function selectAll() {
    setForCheckout([]);
    cart.forEach((element) => {
      setForCheckout((prevValue) => [...prevValue, element.productId]);
    });
  }

  function unselectAll() {
    setForCheckout([]);
  }

  async function removeCart(index) {
    loadingAlert.fire({ title: "Removing from bowl" });
    try {
      const response = await deleteFromCart(cart[index].productId);
      const data = await response.json();
      loadingAlert.close();

      if (data.status === true) {
        const cartResponse = await getUserDetails();
        const cartData = await cartResponse.json();
        setCart(cartData.message.cart);
      } else {
        customAlert.fire({
          title: "An Error occured!",
          icon: "error",
        });
      }
    } catch (e) {
      customAlert.fire({
        title: "An Error occured!",
        icon: "error",
      });
    }
  }

  useEffect(() => {
    getData();
  }, []);

  return user.isAdmin !== true && cart !== null ? (
    <Container>
      <Table size="sm">
        <thead>
          <tr>
            <th>
              {cart.length === forCheckout.length ? (
                <AiOutlineCheckSquare onClick={unselectAll} />
              ) : (
                <AiOutlineBorder onClick={selectAll} />
              )}
            </th>
            <th>Product</th>
            <th>Unit Price</th>
            <th>Quantity</th>
            <th>Total Price</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {products.length > 0
            ? cart.map((product, index) => {
                return (
                  <tr key={index}>
                    <td>
                      {forCheckout.includes(product.productId) ? (
                        <AiOutlineCheckSquare
                          className="svgPointer"
                          onClick={() => removeToCheckout(index)}
                        />
                      ) : (
                        <AiOutlineBorder
                          className="svgPointer"
                          onClick={() => addToCheckout(index)}
                        />
                      )}
                    </td>
                    <td>
                      <Button
                        className="photoHolder"
                        variant="outline"
                        as={Link}
                        to={`/products/${cart[index].productId}`}
                      >
                        <img
                          src={
                            products.find(
                              (item) => item._id === cart[index].productId
                            ).photo
                          }
                          alt="product"
                          className="productPhoto"
                        />
                      </Button>
                      {
                        products.find(
                          (item) => item._id === cart[index].productId
                        ).name
                      }
                    </td>
                    <td>
                      ₱
                      {
                        products.find(
                          (item) => item._id === cart[index].productId
                        ).price
                      }
                    </td>
                    <td>
                      <div className="quantityChanger">
                        <div onClick={() => subtract(product)}>-</div>
                        <div>{product.quantity}</div>
                        <div onClick={() => add(product)}>+</div>
                      </div>
                    </td>
                    <td className="red">
                      ₱
                      {products.find(
                        (item) => item._id === cart[index].productId
                      ).price * product.quantity}
                    </td>
                    <td>
                      <AiOutlineDelete
                        className="svgPointer"
                        size={25}
                        onClick={() => removeCart(index)}
                      />
                    </td>
                  </tr>
                );
              })
            : null}
        </tbody>
        <tfoot>
          <tr className="cartFooter">
            <td>
              {cart.length === forCheckout.length ? (
                <AiOutlineCheckSquare onClick={unselectAll} />
              ) : (
                <AiOutlineBorder onClick={selectAll} />
              )}
            </td>
            <td>Select All ({cart.length})</td>
            <td></td>
            <td colspan="2" className="total-cart">
              Total ({forCheckout.length}
              {forCheckout.length > 1 ? " items" : " item"}):{" "}
              <span>
                ₱
                {forCheckout.reduce((total, productId) => {
                  const cartItem = cart.find(
                    (item) => item.productId === productId
                  );
                  const productItem = products.find(
                    (item) => item._id === productId
                  );

                  if (cartItem && productItem) {
                    return total + cartItem.quantity * productItem.price;
                  } else {
                    return total;
                  }
                }, 0)}
              </span>
            </td>
            <td>
              {forCheckout.length > 0 ? (
                <Button
                  variant="outline-secondary"
                  className="buyNowButton fullwidth"
                  as={Link}
                  to="/checkout"
                >
                  <ImSpoonKnife size={25} fill="white" />
                  &nbsp;Eat Now
                </Button>
              ) : (
                <Button
                  variant="outline-secondary"
                  className="buyNowButton fullwidth"
                  disabled
                >
                  <ImSpoonKnife size={25} fill="white" />
                  &nbsp;Eat Now
                </Button>
              )}
            </td>
          </tr>
        </tfoot>
      </Table>
    </Container>
  ) : (
    <Navigate to="/" />
  );
}
