import { useState, useEffect, useContext } from "react";
import UserContext from "../UserContext";

import { Container, Card, Row, Col } from "react-bootstrap";

import { getUserOrders, getAllActiveProducts } from "../api";
import { customAlert, loadingAlert } from "../utilities/swal";

export default function Profile() {
  const { user } = useContext(UserContext);
  const [orders, setOrders] = useState({});
  const [products, setProducts] = useState([]);

  async function getData() {
    loadingAlert.fire({ title: "Retrieving user data" });
    try {
      const response = await getUserOrders();
      const data = await response.json();

      if (data.status === true) {
        setOrders(data.message);
        const prodResponse = await getAllActiveProducts();
        const prodData = await prodResponse.json();
        loadingAlert.close();

        setProducts(prodData.message);
      } else {
        customAlert.fire({
          title: "An Error occured!",
          icon: "error",
        });
      }
    } catch (e) {
      loadingAlert.close();
      customAlert.fire({
        title: "An Error occured!",
        icon: "error",
      });
    }
  }

  useEffect(() => {
    getData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Container>
      <Card className="profileCard">
        <Card.Body>
          <Card.Title>
            {user.firstName} {user.lastName} <span>{user.email}</span>
          </Card.Title>
          <Card.Subtitle>{user.mobileNo}</Card.Subtitle>
        </Card.Body>
      </Card>
      {products.length > 0
        ? orders
            .slice(0)
            .reverse()
            .map((order, index) => {
              return (
                <Card className="orderCard">
                  <Card.Body>
                    <Card.Title>
                      Order No: {order._id}{" "}
                      <span>
                        {order.purchasedOn.slice(0, 16).replace("T", " ")}
                      </span>
                    </Card.Title>
                    {order.products.map((orderItem, itemIndex) => {
                      return (
                        <Row className="align-items-center">
                          <Col className="orderSpacer">
                            <img
                              src={
                                products.find(
                                  (product) =>
                                    product._id === orderItem.productId
                                ).photo
                              }
                              alt="product"
                              className="productCheckoutPhoto"
                            />
                            {
                              products.find(
                                (product) => product._id === orderItem.productId
                              ).name
                            }
                            &nbsp;x{orderItem.quantity}
                          </Col>
                          <Col>
                            <span>
                              ₱
                              {
                                products.find(
                                  (product) =>
                                    product._id === orderItem.productId
                                ).price
                              }
                            </span>
                          </Col>
                        </Row>
                      );
                    })}
                    <hr />
                    <Card.Text>
                      <Row>
                        <Col>Delivered To: {order.deliveryAddress}</Col>
                        <Col>
                          <span>
                            Order Total: &nbsp;
                            <span id="profileTotal">
                              ₱{order.totalAmount + order.shippingFee}
                            </span>
                          </span>
                        </Col>
                      </Row>
                    </Card.Text>
                  </Card.Body>
                </Card>
              );
            })
        : null}
    </Container>
  );
}
