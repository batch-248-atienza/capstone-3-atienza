import { Container, Button } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function Home() {
  return (
    <>
      <div id="headerContainer"></div>
      <div className="d-flex flex-column align-items-center justify-content-center mt-5">
        <h2 className="my-3">Explore Our Delicious Products</h2>
        <p>
          From classic Halo halo to exotic balut, we've got everything you'll
          need to satisfy your cravings.
        </p>
        <Button
          className="text-white"
          variant="secondary"
          as={Link}
          to={"/products"}
        >
          Browse Our Products
        </Button>
      </div>
      <div className="bg-dark position-fixed w-100 bottom-0">
        <Container
          id="homeFooter"
          className="justify-content-center d-flex my-2"
        >
          © 2023 Turo-Turo Treats. All Rights Reserved.
        </Container>
      </div>
    </>
  );
}
