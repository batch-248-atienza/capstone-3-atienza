import { useState, useEffect, useContext } from "react";
import { Container, Form, Button, Row, Col } from "react-bootstrap";
import { Navigate } from "react-router-dom";
import { AiOutlineEye, AiOutlineEyeInvisible } from "react-icons/ai";
import UserContext from "../UserContext";
import Register from "../components/Register";

import { login, getUserDetails, googleLogin } from "../api";
import { customAlert, loadingAlert } from "../utilities/swal";
import { GoogleLogin } from "@react-oauth/google";

export default function Login() {
  const { user, setUser, showRegisterModal, setShowRegisterModal } =
    useContext(UserContext);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isActive, setIsActive] = useState(false);
  const [showPassword, setShowPassword] = useState(false);

  async function authenticate(e) {
    e.preventDefault();
    loadingAlert.fire({ title: "Logging In" });

    try {
      const response = await login(email, password);
      const data = await response.json();
      loadingAlert.close();
      console.log(data);

      if (data.status === true) {
        localStorage.setItem("token", data.message);
        const userDetailsResponse = await getUserDetails(data.message);
        const userDetailsData = await userDetailsResponse.json();
        loadingAlert.close();
        setUser({
          id: userDetailsData.message._id,
          isAdmin: userDetailsData.message.isAdmin,
          firstName: userDetailsData.message.firstName,
          lastName: userDetailsData.message.lastName,
          mobileNo: userDetailsData.message.mobileNo,
          email: userDetailsData.message.email,
        });

        customAlert.fire({
          title: "Login Successful!",
          icon: "success",
        });
      } else {
        customAlert.fire({
          title: "Login Failed!",
          icon: "error",
        });
      }

      setEmail("");
      setPassword("");
    } catch (e) {
      loadingAlert.close();
      customAlert.fire({
        title: "Login Failed!",
        icon: "error",
      });

      setEmail("");
      setPassword("");
    }
  }

  const handleClick = () => setShowRegisterModal(true);

  const handleShowPassword = () => {
    setShowPassword((prevValue) => !prevValue);
  };

  useEffect(() => {
    if (email !== "" && password !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  return user.id !== null ? (
    user.isAdmin !== true ? (
      <Navigate to="/" />
    ) : (
      <Navigate to="/dashboard" />
    )
  ) : (
    <Container>
      <Row>
        <Col xs={1} md={3} xl={1}></Col>
        <Col xs={10} md={6} xl={3}>
          <h3 id="loginText">
            Login and get your hands on some mouth-watering Filipino street
            food, only at Turo-Turo Treats!
          </h3>
        </Col>
        <Col xs={1} md={3} xl={0}></Col>
        <Col xs={1} md={3} xl={1}></Col>
        <Col xs={10} md={6} xl={4}>
          <div>
            <Form key="login" onSubmit={(e) => authenticate(e)} id="loginForm">
              <Form.Group className="mb-2 w-100" controlId="userEmail">
                <Form.Control
                  type="email"
                  placeholder="Email"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                  required
                />
              </Form.Group>
              <Form.Group className="mb-3 w-100 password" controlId="password">
                <Form.Control
                  type={showPassword ? "text" : "password"}
                  placeholder="Password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                  required
                />
                {showPassword ? (
                  <AiOutlineEyeInvisible
                    className="passwordEyeIcon"
                    onClick={handleShowPassword}
                  />
                ) : (
                  <AiOutlineEye
                    className="passwordEyeIcon"
                    onClick={handleShowPassword}
                  />
                )}
              </Form.Group>
              {isActive ? (
                <Button className="w-100" type="submit" id="submitButton">
                  Log In
                </Button>
              ) : (
                <Button
                  className="w-100"
                  type="submit"
                  id="submitButton"
                  disabled
                >
                  Log In
                </Button>
              )}
              <GoogleLogin
                onSuccess={async (credentialResponse) => {
                  console.log(credentialResponse);
                  const response = await googleLogin(credentialResponse);
                  const data = await response.json();

                  if (data.status === true) {
                    localStorage.setItem("token", data.message);
                    const userResponse = await getUserDetails(data.message);
                    const userData = await userResponse.json();
                    console.log(userData);
                    setUser({
                      id: userData.message._id,
                      isAdmin: userData.message.isAdmin,
                      firstName: userData.message.firstName,
                      lastName: userData.message.lastName,
                      mobileNo: userData.message.mobileNo,
                      email: userData.message.email,
                    });
                    customAlert.fire({
                      title: "Login Successful!",
                      icon: "success",
                    });
                  } else {
                    customAlert.fire({
                      title: "An Error Occured!",
                      icon: "error",
                    });
                  }
                }}
                onError={() => {
                  console.log("Login Failed");
                  customAlert.fire({
                    title: "An Error Occured!",
                    icon: "error",
                  });
                }}
                useOneTap
              />
              <hr className="w-100" />
              <p>Not yet registered?</p>
              <Button id="registerButton" onClick={handleClick}>
                Create an Account
              </Button>
            </Form>
            <Register show={showRegisterModal} />
          </div>
        </Col>
        <Col xs={1} md={3} xl={2}></Col>
      </Row>
    </Container>
  );
}
