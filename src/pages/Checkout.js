import { useState, useEffect, useContext } from "react";
import UserContext from "../UserContext";
import { Navigate } from "react-router-dom";

import {
  Container,
  Card,
  Button,
  ButtonGroup,
  Dropdown,
  Row,
  Col,
} from "react-bootstrap";
import { FiMapPin } from "react-icons/fi";

import {
  getRegions,
  getProvinces,
  getCities,
  getAllActiveProducts,
  checkoutCart,
  deleteFromCart,
  getUserDetails,
} from "../api";
import { customAlert, loadingAlert } from "../utilities/swal";

export default function Checkout() {
  const { user, cart, setCart, forCheckout, setForCheckout } =
    useContext(UserContext);
  const [region, setRegion] = useState({});
  const [regions, setRegions] = useState({});
  const [province, setProvince] = useState({});
  const [provinces, setProvinces] = useState({});
  const [city, setCity] = useState({});
  const [cities, setCities] = useState({});

  const [products, setProducts] = useState([]);
  const [orderDone, setOrderDone] = useState(false);

  async function getData() {
    loadingAlert.fire({ title: "Retrieving bowl" });
    const regionsResponse = await getRegions();
    const regionsData = await regionsResponse.json();
    setRegions(regionsData.data);

    const provincesResponse = await getProvinces();
    const provincesData = await provincesResponse.json();
    setProvinces(provincesData.data);

    const citiesResponse = await getCities();
    const citiesData = await citiesResponse.json();
    setCities(citiesData.data);

    const response = await getAllActiveProducts();
    const data = await response.json();

    if (data.status === true) {
      setProducts(data.message);
    } else {
      setProducts(null);
    }
    loadingAlert.close();
  }

  async function handleCheckout() {
    loadingAlert.fire({ title: "Checking Out" });
    try {
      const response = await checkoutCart(
        forCheckout,
        forCheckout.reduce((total, productId) => {
          const cartItem = cart.find((item) => item.productId === productId);
          const productItem = products.find((item) => item._id === productId);

          if (cartItem && productItem) {
            return total + cartItem.quantity * productItem.price;
          } else {
            return total;
          }
        }, 0),
        50,
        `${city.name}, ${province.name}, ${region.name}`
      );
      const data = await response.json();
      loadingAlert.close();

      if (data.status === true) {
        //remove forCheckout items in cart
        for (let i = 0; i < forCheckout.length; i++) {
          if (
            cart.find((item) => item.productId === forCheckout[i]) !== undefined
          ) {
            await deleteFromCart(cart[i].productId);
          }
        }
        //clear forCheckout state
        setForCheckout([]);

        //update cart state
        const cartResponse = await getUserDetails();
        const cartData = await cartResponse.json();
        setCart(cartData.message.cart);
        setOrderDone(true);

        customAlert.fire({
          title: "Checkout Successful!",
          icon: "success",
        });
      } else {
        customAlert.fire({
          title: "An Error occured!",
          icon: "error",
        });
      }
    } catch (e) {
      loadingAlert.close();
      customAlert.fire({
        title: "An Error occured!",
        icon: "error",
      });
      console.log(e.message);
    }
  }

  useEffect(() => {
    getData();
  }, []);

  return forCheckout.length === 0 ||
    orderDone === true ||
    user.isAdmin === true ? (
    <Navigate to="/" />
  ) : (
    <Container>
      <Card id="deliveryCard">
        <Card.Body>
          <Card.Title>
            <FiMapPin size={20} fill="red" />
            Delivery Address
          </Card.Title>
          <Card.Subtitle>
            {user.firstName} {user.lastName} {user.mobileNo}
          </Card.Subtitle>
          <Card.Text>
            <Dropdown as={ButtonGroup} className="checkoutDropdown">
              <Button variant="outline-primary">
                {Object.keys(region).length === 0 ? "Region" : region.name}
              </Button>
              <Dropdown.Toggle split variant="outline-primary" />
              <Dropdown.Menu>
                {regions.length > 0
                  ? regions.map((region, index) => {
                      return (
                        <Dropdown.Item onClick={() => setRegion(region)}>
                          {region.name}
                        </Dropdown.Item>
                      );
                    })
                  : null}
              </Dropdown.Menu>
            </Dropdown>
            <Dropdown
              as={ButtonGroup}
              className="checkoutDropdown centerCheckoutDropdown"
            >
              <Button variant="outline-primary">
                {Object.keys(province).length === 0
                  ? "Province"
                  : province.name}
              </Button>
              <Dropdown.Toggle split variant="outline-primary" />
              <Dropdown.Menu>
                {provinces.length > 0 && Object.keys(region).length > 0
                  ? provinces.map((province, index) => {
                      if (
                        province.region_code ===
                        regions.find(
                          (singleRegion) => singleRegion.name === region.name
                        ).id
                      ) {
                        return (
                          <Dropdown.Item onClick={() => setProvince(province)}>
                            {province.name}
                          </Dropdown.Item>
                        );
                      } else {
                        return null;
                      }
                    })
                  : null}
              </Dropdown.Menu>
            </Dropdown>
            <Dropdown as={ButtonGroup} className="checkoutDropdown">
              <Button variant="outline-primary">
                {Object.keys(city).length === 0 ? "City" : city.name}
              </Button>
              <Dropdown.Toggle split variant="outline-primary" />
              <Dropdown.Menu>
                {cities.length > 0 &&
                Object.keys(region).length > 0 &&
                Object.keys(province).length > 0
                  ? cities.map((city, index) => {
                      if (
                        city.province_code ===
                        provinces.find(
                          (singleProvince) =>
                            singleProvince.name === province.name
                        ).id
                      ) {
                        return (
                          <Dropdown.Item onClick={() => setCity(city)}>
                            {city.name}
                          </Dropdown.Item>
                        );
                      } else {
                        return null;
                      }
                    })
                  : null}
              </Dropdown.Menu>
            </Dropdown>
          </Card.Text>
        </Card.Body>
      </Card>
      <Card>
        <Card.Body>
          <Row>
            <Col sm={3}>Products Ordered</Col>
            <Col sm={3}>Unit Price</Col>
            <Col sm={3}>Amount</Col>
            <Col sm={3}>Item Subtotal</Col>
          </Row>
          {products.length > 0
            ? forCheckout.map((item, index) => {
                return (
                  <Row className="align-items-center">
                    <Col sm={3}>
                      <img
                        src={
                          products.find((product) => product._id === item).photo
                        }
                        alt="product"
                        className="productCheckoutPhoto"
                      />
                      {products.find((product) => product._id === item).name}
                    </Col>
                    <Col sm={3}>
                      ₱{products.find((product) => product._id === item).price}
                    </Col>
                    <Col sm={3}>
                      {
                        cart.find((product) => product.productId === item)
                          .quantity
                      }
                    </Col>
                    <Col sm={3}>
                      ₱
                      {products.find((product) => product._id === item).price *
                        cart.find((product) => product.productId === item)
                          .quantity}
                    </Col>
                  </Row>
                );
              })
            : null}
          <hr />
          <Row>
            <Col sm={3}></Col>
            <Col sm={3}></Col>
            <Col sm={3}></Col>
            <Col sm={3}>
              <Row>
                <Col sm={6}>Items Subtotal:</Col>
                <Col sm={6}>
                  ₱
                  {forCheckout.reduce((total, productId) => {
                    const cartItem = cart.find(
                      (item) => item.productId === productId
                    );
                    const productItem = products.find(
                      (item) => item._id === productId
                    );

                    if (cartItem && productItem) {
                      return total + cartItem.quantity * productItem.price;
                    } else {
                      return total;
                    }
                  }, 0)}
                </Col>
              </Row>
              <Row>
                <Col sm={6}>Shipping Fee:</Col>
                <Col sm={6}>₱{Object.keys(city).length > 0 ? 50 : null}</Col>
              </Row>
              <Row>
                <Col sm={6}>Total Payment:</Col>
                <Col sm={6} className="checkoutTotal">
                  ₱
                  {Object.keys(city).length > 0
                    ? forCheckout.reduce((total, productId) => {
                        const cartItem = cart.find(
                          (item) => item.productId === productId
                        );
                        const productItem = products.find(
                          (item) => item._id === productId
                        );

                        if (cartItem && productItem) {
                          return total + cartItem.quantity * productItem.price;
                        } else {
                          return total;
                        }
                      }, 0) + 50
                    : null}
                </Col>
              </Row>
            </Col>
          </Row>
          <hr />
          <Row>
            <Col sm={3}></Col>
            <Col sm={3}></Col>
            <Col sm={3}></Col>
            <Col sm={3}>
              {Object.keys(city).length > 0 ? (
                <Button
                  variant="outline-secondary"
                  className="buyNowButton fullwidth"
                  onClick={handleCheckout}
                >
                  Place Order
                </Button>
              ) : (
                <Button
                  disabled
                  variant="outline-secondary"
                  className="buyNowButton fullwidth"
                >
                  Place Order
                </Button>
              )}
            </Col>
          </Row>
        </Card.Body>
      </Card>
    </Container>
  );
}
