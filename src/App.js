import { useState, useEffect } from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

import { UserProvider } from "./UserContext";
import AppNavBar from "./components/AppNavBar";
import ProductView from "./components/ProductView";

import { getUserDetails } from "./api";

import Home from "./pages/Home";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Dashboard from "./pages/Dashboard";
import Products from "./pages/Products";
import Cart from "./pages/Cart";
import Checkout from "./pages/Checkout";
import Profile from "./pages/Profile";
import Users from "./pages/Users";
import Orders from "./pages/Orders";

import "./App.css";

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
    firstName: null,
    lastName: null,
    mobileNo: null,
    email: null,
  });
  const [cart, setCart] = useState([]);
  const [showRegisterModal, setShowRegisterModal] = useState(false);
  const [forCheckout, setForCheckout] = useState([]);

  const unsetUser = () => {
    localStorage.clear();
  };

  async function getDetails() {
    const response = await getUserDetails();
    const data = await response.json();

    if (data.status === true) {
      setCart(data.message.cart);
      setUser({
        id: data.message._id,
        isAdmin: data.message.isAdmin,
        firstName: data.message.firstName,
        lastName: data.message.lastName,
        mobileNo: data.message.mobileNo,
        email: data.message.email,
      });
    } else {
      setCart(null);
      setUser({
        id: null,
        isAdmin: null,
        firstName: null,
        lastName: null,
        mobileNo: null,
        email: null,
      });
    }
  }

  useEffect(() => {
    getDetails();
  }, []);

  return (
    <UserProvider
      value={{
        user,
        setUser,
        unsetUser,
        showRegisterModal,
        setShowRegisterModal,
        cart,
        setCart,
        forCheckout,
        setForCheckout,
      }}
    >
      <Router>
        <AppNavBar />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/dashboard" element={<Dashboard />} />
          <Route path="/dashboard/users" element={<Users />} />
          <Route path="/dashboard/orders" element={<Orders />} />
          <Route path="/products" element={<Products />} />
          <Route path="/products/:productId" element={<ProductView />} />
          <Route path="/login" element={<Login />} />
          <Route path="/logout" element={<Logout />} />
          <Route path="/cart" element={<Cart />} />
          <Route path="/checkout" element={<Checkout />} />
          <Route path="/profile" element={<Profile />} />
        </Routes>
      </Router>
    </UserProvider>
  );
}

export default App;
