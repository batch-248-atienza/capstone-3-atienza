import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";
import { GoogleOAuthProvider } from "@react-oauth/google";

import "bootstrap/dist/css/bootstrap.min.css";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <GoogleOAuthProvider clientId="955063037381-v0bplrlpgcletn2jkdj7thrrk06h41bl.apps.googleusercontent.com">
    <App />
  </GoogleOAuthProvider>
);
