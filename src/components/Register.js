import { useState, useEffect, useContext } from "react";
import { Form, Button, Row, Col } from "react-bootstrap";
import Modal from "react-bootstrap/Modal";
import UserContext from "../UserContext";

import { register } from "../api";
import { customAlert, loadingAlert } from "../utilities/swal";

import {
  MdOutlineCheckBoxOutlineBlank,
  MdOutlineCheckBox,
} from "react-icons/md";

export default function Register(props) {
  const { setShowRegisterModal } = useContext(UserContext);

  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [mobileNo, setMobileNo] = useState("");
  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");

  const [passwordsMatch, setPasswordsMatch] = useState(false);
  const [isPasswordLengthEnough, setIsPasswordLengthEnough] = useState(false);
  const [passwordHasUppercase, setPasswordHasUppercase] = useState(false);
  const [passwordHasLowercase, setPasswordHasLowercase] = useState(false);
  const [passwordHasDigit, setPasswordHasDigit] = useState(false);
  const [isButtonDisabled, setIsButtonDisabled] = useState(true);

  const handleClose = () => setShowRegisterModal(false);

  async function registerUser(e) {
    e.preventDefault();
    loadingAlert.fire({ title: "Registering" });

    try {
      const response = await register(
        firstName,
        lastName,
        email,
        password1,
        mobileNo
      );
      const data = await response.json();
      loadingAlert.close();

      if (data.status === true) {
        customAlert.fire({
          title: "Registration Successful!",
          icon: "success",
        });
      } else {
        customAlert.fire({
          title: "Registration Unsuccessful!",
          icon: "error",
        });
      }

      setFirstName("");
      setLastName("");
      setMobileNo("");
      setEmail("");
      setPassword1("");
      setPassword2("");

      handleClose();
    } catch (e) {
      loadingAlert.close();
      customAlert.fire({
        title: "Registration Unsuccessful!",
        icon: "error",
      });

      setFirstName("");
      setLastName("");
      setMobileNo("");
      setEmail("");
      setPassword1("");
      setPassword2("");
    }
  }

  useEffect(() => {
    password1 === password2 && password1 !== ""
      ? setPasswordsMatch(true)
      : setPasswordsMatch(false);
  }, [password1, password2]);

  useEffect(() => {
    password1.length >= 8
      ? setIsPasswordLengthEnough(true)
      : setIsPasswordLengthEnough(false);

    const testUppercase = /[A-Z]/;
    testUppercase.test(password1)
      ? setPasswordHasUppercase(true)
      : setPasswordHasUppercase(false);

    const testLowercase = /[a-z]/;
    testLowercase.test(password1)
      ? setPasswordHasLowercase(true)
      : setPasswordHasLowercase(false);

    const testHasDigit = /[0-9]/;
    testHasDigit.test(password1)
      ? setPasswordHasDigit(true)
      : setPasswordHasDigit(false);
  }, [password1]);

  useEffect(() => {
    passwordsMatch &&
    isPasswordLengthEnough &&
    passwordHasUppercase &&
    passwordHasLowercase &&
    passwordHasDigit
      ? setIsButtonDisabled(false)
      : setIsButtonDisabled(true);
  }, [
    passwordsMatch,
    isPasswordLengthEnough,
    passwordHasUppercase,
    passwordHasLowercase,
    passwordHasDigit,
  ]);

  return (
    <Modal show={props.show} onHide={handleClose} centered>
      <Modal.Header>
        <Modal.Title>Sign Up</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form
          key="register"
          onSubmit={(e) => registerUser(e)}
          id="registerForm"
        >
          <Row>
            <Form.Group as={Col} className="mb-2" controlId="firstName">
              <Form.Control
                type="firstName"
                placeholder="First Name"
                value={firstName}
                onChange={(e) => setFirstName(e.target.value)}
                required
              />
            </Form.Group>
            <Form.Group as={Col} className="mb-2" controlId="lastName">
              <Form.Control
                type="lastName"
                placeholder="Last Name"
                value={lastName}
                onChange={(e) => setLastName(e.target.value)}
                required
              />
            </Form.Group>
          </Row>
          <Form.Group className="mb-2" controlId="email">
            <Form.Control
              type="email"
              placeholder="Email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              required
            />
          </Form.Group>
          <Form.Group className="mb-2" controlId="mobileNo">
            <Form.Control
              type="text"
              placeholder="Mobile Number"
              value={mobileNo}
              onChange={(e) => setMobileNo(e.target.value)}
              required
            />
          </Form.Group>
          <Row>
            <Form.Group as={Col} className="mb-2" controlId="password1">
              <Form.Control
                type="password"
                placeholder="Password"
                value={password1}
                onChange={(e) => setPassword1(e.target.value)}
                required
              />
            </Form.Group>
            <Form.Group as={Col} className="mb-2" controlId="password2">
              <Form.Control
                type="password"
                placeholder="Re-enter Password"
                value={password2}
                onChange={(e) => setPassword2(e.target.value)}
                required
              />
            </Form.Group>
          </Row>
          <div className="mb-2">
            <div>
              {passwordsMatch ? (
                <MdOutlineCheckBox />
              ) : (
                <MdOutlineCheckBoxOutlineBlank />
              )}
              <span>Passwords must match</span>
            </div>
            <div>
              {isPasswordLengthEnough ? (
                <MdOutlineCheckBox />
              ) : (
                <MdOutlineCheckBoxOutlineBlank />
              )}
              <span>Password must be at least 8 characters</span>
            </div>
            <div>
              {passwordHasUppercase ? (
                <MdOutlineCheckBox />
              ) : (
                <MdOutlineCheckBoxOutlineBlank />
              )}
              <span>Password must include at least 1 uppercase chatacter</span>
            </div>
            <div>
              {passwordHasLowercase ? (
                <MdOutlineCheckBox />
              ) : (
                <MdOutlineCheckBoxOutlineBlank />
              )}
              <span>Password must include at least 1 lowercase chatacter</span>
            </div>
            <div>
              {passwordHasDigit ? (
                <MdOutlineCheckBox />
              ) : (
                <MdOutlineCheckBoxOutlineBlank />
              )}
              <span>Password must include at least 1 digit </span>
            </div>
          </div>
          <Button
            className="w-100"
            type="submit"
            id="submitButton"
            disabled={isButtonDisabled}
          >
            Register
          </Button>
        </Form>
      </Modal.Body>
    </Modal>
  );
}
