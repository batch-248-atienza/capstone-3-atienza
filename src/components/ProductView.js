/* eslint-disable react-hooks/exhaustive-deps */
import { useState, useEffect, useContext } from "react";
import { useParams, Navigate } from "react-router-dom";
import { Container, Row, Col, Card, Button } from "react-bootstrap";

import { getSpecificProduct, addToCart, getUserDetails } from "../api";
import { customAlert, loadingAlert } from "../utilities/swal";
import UserContext from "../UserContext";

import { TbBowl } from "react-icons/tb";
import { ImSpoonKnife } from "react-icons/im";

export default function AppNProductViewavBar() {
  const { user, setCart, setForCheckout } = useContext(UserContext);
  const [product, setProduct] = useState({});
  const [orders, setOrders] = useState([]);
  const [quantity, setQuantity] = useState(1);
  const [boughtSomething, setBoughtSomething] = useState(false);
  const { productId } = useParams();

  const subtract = () => {
    if (quantity !== 1) {
      setQuantity((prevValue) => prevValue - 1);
    }
  };

  const add = () => {
    setQuantity((prevValue) => prevValue + 1);
  };

  async function handleClick() {
    loadingAlert.fire({ title: "Adding to Bowl" });
    try {
      const response = await addToCart(productId, quantity);
      const data = await response.json();

      if (data.status === true) {
        const response = await getUserDetails();
        const data = await response.json();
        loadingAlert.close();

        setCart(data.message.cart);
        customAlert.fire({
          title: "Added to Bowl Successfully!",
          icon: "success",
        });
      } else {
        loadingAlert.close();

        customAlert.fire({
          title: "An Error occured!",
          icon: "error",
        });
      }
    } catch (e) {
      loadingAlert.close();
      customAlert.fire({
        title: "An Error occured!",
        icon: "error",
      });
    }
  }

  async function handleBuy() {
    loadingAlert.fire({ title: "Checking Out" });
    try {
      const response = await addToCart(productId, quantity);
      const data = await response.json();

      if (data.status === true) {
        const response = await getUserDetails();
        const data = await response.json();
        loadingAlert.close();

        setCart(data.message.cart);
        setForCheckout([productId]);
        setBoughtSomething(true);
      } else {
        loadingAlert.close();
        customAlert.fire({
          title: "An Error occured!",
          icon: "error",
        });
      }
    } catch (e) {
      loadingAlert.close();
      customAlert.fire({
        title: "An Error occured!",
        icon: "error",
      });
    }
  }

  async function getData() {
    loadingAlert.fire({ title: "Retrieving data" });
    try {
      const response = await getSpecificProduct(productId);
      const data = await response.json();
      loadingAlert.close();

      if (data.status === true) {
        setProduct(data.message);
        setOrders(data.message.orders);
      } else {
        setProduct(null);
        setOrders(null);
      }
    } catch (e) {
      loadingAlert.close();
      customAlert.fire({
        title: "An Error occured!",
        icon: "error",
      });
    }
  }

  useEffect(() => {
    getData();
  }, []);

  return boughtSomething === true ? (
    <Navigate to="/checkout" />
  ) : (
    <Container>
      <Card>
        <Row>
          <Col sm={5}>
            <Card.Body>
              <Card.Img src={product.photo} />
            </Card.Body>
          </Col>
          <Col sm={7}>
            <Card.Body>
              <Card.Title>{product.name}</Card.Title>
              <Card.Text>
                {orders !== null
                  ? orders.reduce((total, order) => total + order.quantity, 0) +
                    " sold"
                  : 0 + " sold"}
              </Card.Text>
              <Card.Subtitle id="productViewPrice">
                ₱{product.price}
              </Card.Subtitle>
              <Row className="productSpacer">
                <Col sm={2}>Description</Col>
                <Col sm={10}>{product.description}</Col>
              </Row>
              <Row className="productSpacer">
                <Col sm={2}>Quantity</Col>
                <Col sm={10} className="quantityChanger">
                  <div onClick={subtract}>-</div>
                  <div>{quantity}</div>
                  <div onClick={add}>+</div>
                </Col>
              </Row>
              <Row>
                <Col>
                  {user.isAdmin === false ? (
                    <>
                      <Button
                        variant="outline-secondary"
                        className="addToCartButton"
                        onClick={handleClick}
                      >
                        {" "}
                        <TbBowl size={25} fill="white" />
                        Add To Bowl
                      </Button>
                      <Button className="buyNowButton" onClick={handleBuy}>
                        <ImSpoonKnife size={25} fill="white" />
                        &nbsp;Eat Now
                      </Button>
                    </>
                  ) : (
                    <>
                      <Button
                        variant="outline-secondary"
                        className="addToCartButton"
                        disabled
                      >
                        {" "}
                        <TbBowl size={25} fill="white" />
                        &nbsp;Add To Bowl
                      </Button>
                      <Button className="buyNowButton" disabled>
                        <ImSpoonKnife size={25} fill="white" />
                        &nbsp;Eat Now
                      </Button>
                    </>
                  )}
                </Col>
              </Row>
            </Card.Body>
          </Col>
        </Row>
      </Card>
    </Container>
  );
}
