import { useContext } from "react";
import { Navbar, Nav, Container, Form } from "react-bootstrap";
import { Link } from "react-router-dom";

import { ReactComponent as Logo } from "../logo.svg";
import UserContext from "../UserContext";
import Register from "./Register";

import { AiOutlineSearch, AiOutlineUser } from "react-icons/ai";
import { TbBowl } from "react-icons/tb";

export default function AppNavBar() {
  const { user, showRegisterModal, setShowRegisterModal, cart } =
    useContext(UserContext);

  const handleClick = () => setShowRegisterModal(true);

  return (
    <>
      <Navbar expand="md">
        <Container fluid>
          <Navbar.Brand as={Link} to="/">
            <svg height="40px" width="100px">
              <Logo />
            </svg>
          </Navbar.Brand>
          <Form id="searchbar">
            <Form.Control type="search" placeholder="Search" />
            <AiOutlineSearch size={25} />
          </Form>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav>
              <Nav.Link as={Link} to="/products">
                Products
              </Nav.Link>
              {user.isAdmin === true && (
                <Nav.Link as={Link} to="/dashboard">
                  Dashboard
                </Nav.Link>
              )}
              {user.id !== null ? (
                <Nav.Link as={Link} to="/logout">
                  Logout
                </Nav.Link>
              ) : (
                <>
                  <Nav.Link as={Link} to="/login">
                    Login
                  </Nav.Link>
                  <Nav.Link onClick={handleClick}>Register</Nav.Link>
                </>
              )}
            </Nav>
          </Navbar.Collapse>
          {user.id !== null ? (
            <Nav id="profile" as={Link} to="/profile">
              <AiOutlineUser size={25} />
            </Nav>
          ) : null}
          {user.isAdmin !== true && user.id !== null ? (
            <Nav id="cart" as={Link} to="/cart">
              <TbBowl size={25} />
              {cart !== null ? <div>{cart.length}</div> : null}
            </Nav>
          ) : null}
        </Container>
      </Navbar>
      <Register show={showRegisterModal} />
    </>
  );
}
