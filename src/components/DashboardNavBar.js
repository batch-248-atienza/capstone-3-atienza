import { Col } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function DashboardNavBar() {
  return (
    <>
      <Col xs={4} className="mb-3" as={Link} to={`/dashboard`}>
        <div className="border rounded">
          <span className="fw-bold d-table m-auto">Products</span>
        </div>
      </Col>
      <Col xs={4} className="mb-3" as={Link} to={`/dashboard/users`}>
        <div className="border rounded">
          <span className="fw-bold d-table m-auto">Users</span>
        </div>
      </Col>
      <Col xs={4} className="mb-3" as={Link} to={`/dashboard/orders`}>
        <div className="border rounded">
          <span className="fw-bold d-table m-auto">Orders</span>
        </div>
      </Col>
    </>
  );
}
